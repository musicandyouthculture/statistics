# About The Project

einheitliche Diagramme und einfachere Statistiken mit Python, Pandas und Matplotlib

# Getting Started

## Install Python

## Install Packages

- install pip
- ``pip install matplotlib``
- ``pip install pandas``
- ``pip install numpy``

## data.csv

- erstelle eine Datei ``data.csv``, die wie ``data.csv.example`` aufgebaut ist

# Usage

## Member Data

- hier liegen alle Funktionen

## Members 27 and older

- gibt alle Mitglieder aus, die 27 Jahre oder älter sind

## Plot Members Age

- erstellt eine Grafik mit dem Alter

## Plot Members Birthday

- erstellt eine Grafik mit den Geburtstagen

## Plot Members per Year

- erstellt eine Grafik mit den Mitgliedern pro Jahr
