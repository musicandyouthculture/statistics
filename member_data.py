import datetime

import pandas as pd
import matplotlib.pyplot as plt
import numpy as np


class MemberData:

    def __init__(self, csv_path):
        self.data = self.read_from_csv(csv_path)
        self.clean_data()

    def read_from_csv(self, csv_path):
        return pd.read_csv(csv_path, sep=";")

    def clean_data(self):
        self.data["day_of_accession"] = pd.to_datetime(self.data["day_of_accession"])
        self.data["birthday"] = pd.to_datetime(self.data["birthday"])

    def get_count_of_accessions_per_year(self):
        accessions = pd.DataFrame()
        accessions["day_of_accession"] = self.data["day_of_accession"].dt.year
        accessions_per_year = accessions["day_of_accession"].value_counts()
        accessions_per_year.sort_index(inplace=True)
        return accessions_per_year

    def get_count_of_members_per_year(self):
        count_of_accessions_per_year = self.get_count_of_accessions_per_year()
        members_per_year = count_of_accessions_per_year.copy()

        for year in members_per_year.index:
            if year != members_per_year.index.min():
                members_per_year[year] = members_per_year[year] + members_per_year[year - 1]

        return members_per_year

    def plot_count_of_members_per_year(self):
        updated_on = datetime.datetime.now()

        count_of_members_per_year = self.get_count_of_members_per_year()

        x = count_of_members_per_year.index
        y = count_of_members_per_year.tolist()

        statistic_since_year = x.min()
        statistic_until_year = int(updated_on.year)

        title = "Anzahl Mitglieder"
        title += "\nvon {} bis {}".format(statistic_since_year, statistic_until_year)

        x_label = "Jahr"
        y_label = "Anzahl Mitglieder"

        x_lim_min = statistic_since_year
        x_lim_max = statistic_until_year + 1
        y_lim_min = 0
        y_lim_max = max(y) + 10

        fig_text = "aktualisiert am {}".format(updated_on.strftime("%d.%m.%Y"))
        fig_text += "\nausgeschiedene Mitglieder nicht berücksichtigt"

        color = "#af121d"
        marker = "o"

        self.__set_figure_size()

        plt.plot(x, y, marker=marker, color=color)

        plt.grid()

        plt.xlim(x_lim_min, x_lim_max)
        plt.ylim(y_lim_min, y_lim_max)
        plt.xticks(np.arange(x_lim_min, x_lim_max))
        plt.yticks(np.arange(y_lim_min, y_lim_max, 25))

        plt.title(title)
        plt.xlabel(x_label)
        plt.ylabel(y_label)
        self.__annotate_points(x, y)

        self.__set_fig_text(fig_text)

        plt.show()

    def __set_fig_text(self, fig_text):
        plt.figtext(
            0.5,
            0.01,
            fig_text,
            ha="right",
            ma="left"
        )

    def __annotate_points(self, x, y):
        for xy in zip(x, y):
            plt.annotate("%s" % xy[1], xy=xy, textcoords="offset points", xytext=(0, 5), ha="center")

    @staticmethod
    def __set_figure_size():
        figure = plt.figure()
        figure.set_figwidth(6.4 * 1.1)  # default: 6.4
        figure.set_figheight(4.8 * 1.5)  # default:4.8

    def get_count_of_members_birthyear_per_year(self):
        birthyears = pd.DataFrame()
        birthyears["birthday"] = self.data["birthday"].dt.year
        birthyears_per_year = birthyears["birthday"].value_counts()
        birthyears_per_year.sort_index(inplace=True)
        return birthyears_per_year

    def plot_count_of_members_birthyears_per_year(self):
        updated_on = datetime.datetime.now()

        count_of_members_birthyear_per_year = self.get_count_of_members_birthyear_per_year()

        x = count_of_members_birthyear_per_year.index
        y = count_of_members_birthyear_per_year.tolist()

        statistic_since_year = x.min()
        statistic_until_year = int(updated_on.year)

        title = "Geburtsjahr der Mitglieder"

        x_label = "Geburtsjahr"
        y_label = "Anzahl Mitglieder"

        x_lim_min = statistic_since_year - 1
        x_lim_max = statistic_until_year + 1
        y_lim_min = 0
        y_lim_max = max(y) + 5

        fig_text = "aktualisiert am {}".format(updated_on.strftime("%d.%m.%Y"))

        color = "#af121d"

        self.__set_figure_size()

        plt.bar(x, y, color=color)

        plt.grid()

        plt.xlim(x_lim_min, x_lim_max)
        plt.ylim(y_lim_min, 5 * round(y_lim_max / 5))
        plt.xticks(np.arange(5 * round(x_lim_min / 5), x_lim_max, 5))
        plt.yticks(np.arange(y_lim_min, y_lim_max, 5))

        plt.title(title)
        plt.xlabel(x_label)
        plt.ylabel(y_label)
        self.__annotate_points(x, y)

        self.__set_fig_text(fig_text)

        plt.show()

    def get_count_of_members_age(self, today):
        ages = self.__get_members_age(today)
        count_ages = ages["age"].value_counts()
        count_ages.sort_index(inplace=True)
        return count_ages

    def __get_members_age(self, updated_on):
        ages = pd.DataFrame()
        ages["age"] = (updated_on.year - self.data["birthday"].dt.year) - (
                (updated_on.month - self.data["birthday"].dt.month) < 0)
        return ages

    def plot_count_of_members_age(self):
        updated_on = datetime.datetime.now()

        count_of_members_age = self.get_count_of_members_age(updated_on)

        x = count_of_members_age.index
        y = count_of_members_age.tolist()

        title = "Alter der Mitglieder"

        x_label = "Alter"
        y_label = "Anzahl Mitglieder"

        x_lim_min = 0
        x_lim_max = max(x) + 1
        y_lim_min = 0
        y_lim_max = max(y) + 5

        fig_text = "aktualisiert am {}".format(updated_on.strftime("%d.%m.%Y"))

        color = "#af121d"

        self.__set_figure_size()

        plt.bar(x, y, color=color)

        plt.grid()

        plt.xlim(x_lim_min, x_lim_max)
        plt.ylim(y_lim_min, 5 * round(y_lim_max / 5))
        plt.xticks(np.arange(5 * round(x_lim_min / 5), x_lim_max, 5))
        plt.yticks(np.arange(y_lim_min, y_lim_max, 5))

        plt.title(title)
        plt.xlabel(x_label)
        plt.ylabel(y_label)
        self.__annotate_points(x, y)

        self.__set_fig_text(fig_text)

        plt.show()

    def get_all_members_27_and_older(self):
        today = datetime.datetime.now()

        ages = self.__get_members_age(today)

        members_with_age = self.data
        members_with_age["age"] = ages

        members_27_and_older = members_with_age[members_with_age["age"] >= 27]
        members_27_and_older.reset_index(inplace=True)

        return members_27_and_older
